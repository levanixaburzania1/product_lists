import {products} from './Api.js';
import {generateProduct} from "./Product.js";

// fill up the catalog
const fillUpCatalog = async (sort = null, category = null) => {
    const productList = await products(sort);

    let productHTML = '';
    const catalog = document.getElementsByClassName('main__catalog')[0];
    for(const product of productList){
        if(category != null){
            if(category.value==product.category){
                productHTML += generateProduct(product);
            }
        } else {
            productHTML += generateProduct(product);

        }





    }
    catalog.innerHTML = productHTML;

    }

fillUpCatalog();

const sort = document.getElementById("sort");

sort.addEventListener("change",()=>{
    fillUpCatalog(sort.value);
    console.log(sort.value);
})

// handlesearch

const searchQuery = document.getElementById("searchQuery");
const searchButton = document.getElementById("searchButton");

searchButton.addEventListener("click", ()=>{

    products(sort).then(result => {
    const filterProducts = result.filter(product =>
        product.title.toUpperCase().indexOf(searchQuery.value.toUpperCase())!=-1);

            let productHTML = '';
            const catalog = document.getElementsByClassName('main__catalog')[0];
            for(const product of filterProducts){
                productHTML += generateProduct(product);

            }
            catalog.innerHTML = productHTML;
    })



})

// categories

const categories = document.getElementById("categories");

categories.addEventListener("change",()=>{
    fillUpCatalog(sort.value,categories);
console.log(categories.value);

})

