import { SERVER__ADDR } from "./Config.js";

export const call = async (url) => {
  const request = await fetch(SERVER__ADDR + url);
  const result = await request.json();
  return result;
};
